class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :user, null: false, comment: 'User ID'

      t.string :description, null: false, comment: 'Description'
      t.integer :state, null: false, default: 0,
	comment: 'State (:ai, :todo, :archive)'
      t.boolean :recurrent, null: false, default: false,
	comment: 'Whether to keep it in AI'
      t.integer :length, null: false, default: 1, comment: 'Length in pomodoro'
      t.integer :spent, null: false, default: 0,
	comment: 'Pomodoros spent doing task'
      t.boolean :done, null: false, default: false,
	comment: 'Whether task is done'
    end

    add_index :tasks, :description
    add_index :tasks, :state

    add_foreign_key :tasks, :users
  end
end
