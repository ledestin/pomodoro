class AddStreakToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.integer :streak, null: false, default: 0
      t.timestamp :last_pomodoro_ended_at, null: false,
        default: Time.at(0)
    end
  end
end
