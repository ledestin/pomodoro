class AddWorkPartEndedAtAndRestPartEndedAtToUsers < ActiveRecord::Migration[5.2]
  def up
    change_table(:users) do |t|
      t.remove :last_pomodoro_ended_at

      t.timestamp :work_part_started_at
      t.timestamp :work_part_ended_at
    end
  end

  def down
    change_table(:users) do |t|
      t.timestamp :last_pomodoro_ended_at

      t.remove :work_part_started_at
      t.remove :work_part_ended_at
    end
  end
end
