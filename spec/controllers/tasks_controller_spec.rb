require 'rails_helper'

RSpec.describe TasksController, :type => :controller do
  let(:user) { create :user }
  let(:user2) { create :user }
  let(:user2_task) { create :task, user: user2, description: 'lala' }
  let(:another_users_task_id) { user2_task.id }
  let(:errors) { JSON.parse(response.body)['errors'] }

  before :each do
    @ai_task = create :task, user: user, description: 'first'
    @todo_task = create :task, user: user, description: 'buy milk',
      state: :todo
  end

  describe 'access:' do
    it 'blocks unauthenticated access' do
      sign_in nil
      get :index, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'allows authenticated access' do
      sign_in user
      get :index, params: { state: 'ai' }, format: :json
      expect(response).to be_successful
    end
  end

  describe 'GET index' do
    before :each do
      sign_in user
    end

    context 'assigns @tasks for' do
      it 'ai' do
	get :index, params: { state: 'ai' }, format: :json
	expect(assigns(:tasks)).to eq [@ai_task]
      end

      it 'todo' do
	get :index, params: { state: 'todo' }, format: :json
	expect(assigns(:tasks)).to eq [@todo_task]
      end
    end
  end

  describe 'POST create:' do
    let(:description) { 'lala' }

    before :each do
      sign_in user
    end

    it 'successfully creates task' do
      post :create, format: :json,
        params: { task: { description: description, length: 2 } }
      expect(response).to have_http_status(:created)
      expect(errors).to be_nil
      expect(Task.find_by(description: description)).not_to be_nil
    end

    context "doesn't create task" do
      it 'when length is empty' do
	post :create, format: :json,
	  params: { task: { description: description, length: '' } }
	expect(response).to have_http_status(:unprocessable_entity)
	expect(errors).to be_a(Array)
	expect(Task.find_by(description: description)).to be_nil
      end

      it "when description isn't given" do
	post :create, format: :json, params: { task: { length: 2 } }
	expect(response).to have_http_status(:unprocessable_entity)
	expect(errors).to be_a(Array)
	expect(Task.find_by(length: 2)).to be_nil
      end
    end
  end

  describe 'DELETE destroy' do
    before :each do
      sign_in user
    end

    it 'successfully destroys task' do
      task_id = user.tasks.first.id
      delete :destroy, format: :json, params: { id: task_id }
      expect(response).to have_http_status(:no_content)
      expect(Task.find_by(id: task_id)).to be_nil
    end

    it 'fails to destroy a task' do
      delete :destroy, format: :json, params: { id: another_users_task_id }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(errors).to eq ['No such task']
    end
  end

  describe 'PUT update' do
    before :each do
      sign_in user
    end

    it 'successfully updates task' do
      task_id = @ai_task.id
      put :update, format: :json, params: { id: task_id, task: { length: 5 } }
      expect(response).to have_http_status(:no_content)
      expect(Task.find(task_id).length).to eq 5
    end

    it 'reports error if no such task found' do
      put :update, format: :json,
        params: { id: another_users_task_id, task: { length: 5 } }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(errors).to eq ['No such task']
    end

    it 'reports validation errors if any' do
      put :update, format: :json,
        params: { id: @ai_task.id, task: { length: 'ab' } }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(errors).to eq ['Length is not a number']
    end
  end
end
