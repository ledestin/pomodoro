require 'rails_helper'

RSpec.describe SettingsController, type: :controller do
  let(:user) { create :user }
  let(:json_body) { JSON.parse response.body }

  before :each do
    sign_in user

    user.settings(:pomodoro).update_attributes! work_length: 10,
      short_rest_length: 11, long_rest_length: 12
  end

  describe "GET #index" do
    it "should return current settings" do
      get :index, format: :json, xhr: true
      expect(response).to have_http_status(:success)
      expect(json_body["pomodoro"]).to include("work_length" => 10,
                                   "short_rest_length" => 11,
                                   "long_rest_length" => 12)
    end
  end
end
