require 'site_prism'

class SettingsPage < SitePrism::Page
  set_url '/settings'

  element :work_length, '#settings_work_length'
  element :short_rest_length, '#settings_short_rest_length'
  element :long_rest_length, '#settings_long_rest_length'
  element :save_button, '[name="commit"]'
end
