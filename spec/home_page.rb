require 'site_prism'
require 'support/wait_for_ajax'

def focus_selector selector
  "#{selector}:focus"
end

class AddFormSection < SitePrism::Section
  include WaitForAjax

  DESCRIPTION_SELECTOR = '#atf_description'
  LENGTH_SELECTOR = '#atf_length'

  element :label_description, 'label[name="description"]'
  element :description, DESCRIPTION_SELECTOR
  element :label_length, 'label[name="length"]'
  element :length, LENGTH_SELECTOR
  element :add_button, '#atf_submit'
  element :alerts, '#atf_alerts'

  def add_task description, length
    set_fields description, length
    submit
    wait_for_ajax
  end

  def set_fields description, length
    self.description.set description
    self.length.set length
  end

  def submit
    add_button.click
  end
end

class ItemSection < SitePrism::Section
  element :recurrent, 'input.recurrent[type="checkbox"]'
  element :description, 'span[name="task_description"]'
  element :description_input, 'input[name="task_description"]'
  element :length, 'label[name="task_length"]'
  element :length_input, 'input[name="task_length"]'
  element :move_button, 'button[name="move_button"]'
  element :remove_button, 'button[name="remove_button"]'

  def show_length_input
    length.click
  end

  def show_description_input
    description.click
  end
end

class TodoItemSection < ItemSection
  element :done, 'input.done[type="checkbox"]'
  element :spent, 'label[name="spent"]'
  element :start_button, 'button[name="start"]'
end

class ListSection < SitePrism::Section
  element :title, 'h2.taskList'
end

class AiListSection < ListSection
  sections :tasks, ItemSection, 'ul.taskList li'
end

class TodoListSection < ListSection
  sections :tasks, TodoItemSection, 'ul.taskList li'
end

class TimerFormSection < SitePrism::Section
  element :description, AddFormSection::DESCRIPTION_SELECTOR
  element :label_length, 'label[name="length"]'
  element :length, AddFormSection::LENGTH_SELECTOR
  element :add_button, '#atf_submit'

  element :short_rest_button, '#timerRestButton'
  element :long_rest_button, '#timerLongRestButton'
  element :cancel_button, '#timerCancelButton'
  element :return_to_workspace_button, '#timerReturnButton'
end

class TotalsSection < SitePrism::Section
  element :streak, '.streakBadge'
end

class Home < SitePrism::Page
  set_url '/workspace'

  section :add_form, AddFormSection, '#addForm'
  section :ai_list, AiListSection, '#activityInventory'
  section :todo_list, TodoListSection, '#todayTODO'
  section :timer_form, TimerFormSection, '#timerModal'
  section :totals, TotalsSection, '#todayTODOTotals'
  element :call_it_a_day, 'a[href="/workspace/call_it_a_day"]'
end
