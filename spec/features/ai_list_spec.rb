require 'rails_helper'
require 'home_page'

describe 'AI list:', :js => true do
  let(:home_page) { Home.new }
  let(:title) { home_page.ai_list.title }
  def task
    home_page.ai_list.tasks.first
  end

  include_context 'sign_up_and_sign_in'

  before :each do
    # Clear TodayTODO first.
    home_page.todo_list.tasks.first.remove_button.click
  end

  it 'title is there' do
    expect(title.text).not_to be_empty
  end

  describe 'task description' do
    context 'text' do
      it 'is there' do
	expect(task.description.text).to eq 'first'
      end

      it 'clicking on it, replaces it with input' do
	task.show_description_input
	expect { task.description_input }.not_to raise_error
      end
    end

    context 'input' do
      it 'contains value text contained before it was clicked' do
	value = task.description.text
	task.show_description_input
	expect(task.description_input.value).to eq value
      end

      it 'is focused' do
	# has_selector?('...:focus') doesn't work for some reason, so I have to
	# resort to this.
	task.show_description_input
	value = task.evaluate_script 'document.activeElement.value'
	expect(task.description_input.value).to eq value
      end

      it 'switches back to text if clicked outside of it' do
	task.show_description_input
	title.click
	expect(task.has_no_description_input?).to eq true
      end

      it 'allows to set task description' do
	task.show_description_input
	task.description_input.set "lala\n"
	expect(task.description.text).to eq 'lala'
      end

      it 'allows to cancel entered text by pressing ESC' do
	task.show_description_input
	task.description_input.set "lala\e"
	expect(task.description.text).to eq 'first'
      end
    end
  end

  describe 'length' do
    context 'text' do
      it 'is there' do
	expect(task.length.text).to eq 'length 1'
      end

      it 'clicking on it, replaces it with input' do
	task.show_length_input
	expect(task.has_length_input?).to eq true
      end
    end

    context 'input' do
      it 'contains value text contained before it was clicked' do
	text_value = /^length (?<value>\d+)$/.match(task.length.text)[:value]
	task.show_length_input
	expect(task.length_input.value).to eq text_value
      end

      it 'is focused' do
	task.show_length_input
	value = task.evaluate_script 'document.activeElement.value'
	expect(task.length_input.value).to eq value
      end

      it 'switches back to text if clicked outside of it' do
	task.show_length_input
	title.click
	expect(task.has_no_length_input?).to eq true
      end

      it 'allows to set task length' do
	task.show_length_input
	task.length_input.set "2\n"
	expect(task.length.text).to eq 'length 2'
      end

      it 'allows to cancel entered text by pressing ESC' do
	task.show_length_input
	task.length_input.set "2\e"
	expect(task.length.text).to eq 'length 1'
      end
    end
  end

  describe 'recurrent checkbox' do
    let(:checkbox) { task.recurrent }

    it 'is off for the first item' do
      expect(checkbox.checked?).to eq false
    end

    it 'becomes on if clicked on' do
      checkbox.click
      expect(checkbox.checked?).to eq true
    end
  end

  describe 'move button' do
    it 'removes not recurrent item from AI' do
      task.move_button.click
      expect(task.description.text).to eq 'second'
    end

    it 'removes recurrent item from AI' do
      task.recurrent.click
      task.move_button.click
      expect(task.description.text).to eq 'second'
    end

    context 'adds item to TODO' do
      after :each do
	task.move_button.click
	expect(home_page.todo_list.tasks.first.description.text).to eq 'first'
      end

      it 'not recurrent' do
      end

      it 'recurrent' do
	task.recurrent.click
      end
    end
  end

  describe 'remove button' do
    after :each do
      # Ensure remove doesn't mutate TodayTODO.
      expect(home_page.todo_list.tasks).to be_empty
    end

    it 'removes not recurrent item from AI' do
      task.remove_button.click
      expect(task.description.text).to eq 'second'
    end

    it 'removes recurrent item from AI' do
      task.recurrent.click
      task.remove_button.click
      expect(task.description.text).to eq 'second'
    end
  end
end
