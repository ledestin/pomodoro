require 'rails_helper'
require 'settings_page'

describe "Settings page:", js: true do
  let(:settings_page) { SettingsPage.new }

  include_context "sign_up_and_sign_in"

  before do
    settings_page.load
  end

  scenario "Shows default values" do
    expect(settings_page.work_length.value).to match(/^\d+$/)
    expect(settings_page.short_rest_length.value).to match(/^\d+$/)
    expect(settings_page.long_rest_length.value).to match(/^\d+$/)
  end

  feature "Can save settings" do
    let(:new_work_length) { "20" }
    let(:new_short_rest_length) { "10" }
    let(:new_long_rest_length) { "15" }

    scenario "Successfully, when all values are numbers" do
      expect(settings_page.work_length.value).not_to eq new_work_length
      expect(settings_page.short_rest_length.value)
        .not_to eq new_short_rest_length
      expect(settings_page.long_rest_length.value)
        .not_to eq new_long_rest_length

      settings_page.work_length.set new_work_length
      settings_page.short_rest_length.set new_short_rest_length
      settings_page.long_rest_length.set new_long_rest_length
      settings_page.save_button.click

      expect(page).to have_current_path(home_page.url)
      expect(page).to have_text("Successfully saved settings")

      settings_page.load
      expect(settings_page.work_length.value).to eq new_work_length
      expect(settings_page.short_rest_length.value).to eq new_short_rest_length
      expect(settings_page.long_rest_length.value).to eq new_long_rest_length
    end

    scenario "Fail when a validation fails" do
      settings_page.work_length.set "oo"
      settings_page.save_button.click

      expect(page).to have_current_path(settings_page.url)
      expect(page).to have_text("is not a number")

      settings_page.load
      expect(settings_page.work_length.value).not_to eq "oo"
    end
  end
end
