require 'rails_helper'

feature 'Sign in' do
  before :all do
    @user = build :user
    sign_up @user
  end

  scenario 'user signs in successfully' do
    sign_in @user
  end
end
