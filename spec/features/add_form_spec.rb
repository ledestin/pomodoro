require 'rails_helper'
require 'capybara-screenshot/rspec'
require 'home_page'

describe 'Add form:', :js => true do
  let(:form) { home_page.add_form }
  let(:description) { 'lala' }

  include_context 'sign_up_and_sign_in'

  feature 'Clicking on label, focuses on corresponding field:' do
    scenario 'description focuses' do
      form.label_description.click
      selector = focus_selector(AddFormSection::DESCRIPTION_SELECTOR)
      expect(form.has_selector? selector).to eq true
    end

    scenario 'length focuses' do
      form.label_length.click
      selector = focus_selector(AddFormSection::LENGTH_SELECTOR)
      expect(form.has_selector? selector).to eq true
    end
  end

  feature 'Set fields:' do
    context 'Description field' do
      it 'is empty by default' do
	expect(form.description.value).to eq ''
      end

      it 'can be set' do
	form.description.set description
	expect(form.description.value).to eq description
      end
    end

    context 'Length field' do
      DEFAULT_LENGTH = '1'

      it 'is DEFAULT_LENGTH by default' do
	expect(form.length.value).to eq DEFAULT_LENGTH
      end

      scenario 'can be set to a number' do
	length = '2'
	form.length.set length
	expect(form.length.value).to eq length
      end

      scenario 'can be set to an empty string' do
	length = ''
	form.length.set length
	expect(form.length.value).to eq length
      end

      scenario "can't be set to a non-numeric value" do
	form.length.set 'lala'
	expect(form.length.value).to eq DEFAULT_LENGTH
      end

      it "can't be set to a float value" do
	form.length.set '0.1'
	expect(form.length.value).to eq DEFAULT_LENGTH + '1'
      end

      it "after setting it to empty string, non-numeric value can't be set" do
	# Empty length first, so that internal state of React component becomes
	# empty string.
	# ' ' is used so that Selenium would use sendKeys, thus triggering
	# onChange and setting internal state.
	form.length.set ' '
	expect(form.length.value).to eq ''

	form.length.set 'l'
	expect(form.length.value).to eq ''
      end

      it "can't be set to value < 1" do
	form.length.set '0'
	expect(form.length.value).to eq DEFAULT_LENGTH
      end

      it "can't be set to value > 99" do
	form.length.set '100'
	expect(form.length.value).to eq '10'
      end
    end
  end

  feature 'Add task:' do
    def mk_regex description, length
      /#{description}\s+length\s+#{length}/
    end

    scenario 'a successfull add (new task is on top)' do
      length = '2'
      form.add_task description, length
      task = home_page.ai_list.tasks.first
      expect(task.root_element.has_content? \
	mk_regex(description, length)).to eq true
    end

    scenario 'move added task to TODO (on top)' do
      length = '2'
      form.add_task description, length
      home_page.ai_list.tasks.first.move_button.click
      task = home_page.todo_list.tasks.first
      expect(task.done.checked?).to eq false
      expect(task.root_element.has_content? \
        mk_regex(description, length)).to eq true
      expect(task.root_element.has_content? /spent 0/).to eq true
    end

    context 'an unsuccessfull add' do
      it 'with empty name' do
	description, length = '', '2'
	form.add_task description, length
	expect(home_page.ai_list.has_no_content? \
	  mk_regex(description, length)).to eq true
	expect(form.alerts.has_content? "Description can't be").to eq true
      end

      it 'with empty length' do
	length = ''
	form.add_task description, length
	expect(home_page.ai_list.has_no_content? \
	  mk_regex(description, length)).to eq true
	expect(form.alerts.has_content? "Length can't be").to eq true
      end
    end
  end
end
