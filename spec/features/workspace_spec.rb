require 'rails_helper'
require 'home_page'

feature 'tasks are saved to backend', js: true do
  include_context 'sign_up_and_sign_in'

  scenario 'when moved from AI to TODO' do
    # Empty todo list, so that the moved task will be first in todo list.
    todo_task.remove_button.click

    # Move task to todo.
    task = home_page.ai_list.tasks.first
    task.move_button.click
    wait_for_ajax

    home_page.load
    wait_for_ajax
    expect(todo_task.description.text).to eq 'first'
  end

  scenario 'when moved from TODO to AI' do
    todo_task.move_button.click
    wait_for_ajax
    home_page.load
    wait_for_ajax
    expect(ai_task.description.text).to eq 'buy milk'
  end
end

feature "pressing 'Call it a day!' doesn't break AI", js: true do
  include_context 'sign_up_and_sign_in'

  scenario 'AI tasks reflect changes to its models' do
    home_page.call_it_a_day.click
    wait_for_ajax
    home_page.ai_list.wait_until_tasks_visible wait: 10, count: 3
    task = home_page.ai_list.tasks[1]
    expect(task.recurrent.checked?).to eq false
    task.recurrent.click
    expect(task.recurrent.checked?).to eq true
  end
end

feature 'recurrent task', js: true do
  include_context 'sign_up_and_sign_in'

  scenario 'stays recurrent after move to TODO and back' do
    # Move to todo list
    task = home_page.ai_list.tasks.first
    expect(task.description.text).to eq 'first'
    task.recurrent.click
    expect(task.recurrent.checked?).to eq true
    task.move_button.click

    # Move back to AI.
    task_in_todo = home_page.todo_list.tasks.first
    task_in_todo.move_button.click

    # Check it's still recurrent.
    task = home_page.ai_list.tasks.first
    expect(task.description.text).to eq 'first'
    expect(task.recurrent.checked?).to eq true
  end
end

feature 'Pomodoro', js: true do
  include_context 'sign_up_and_sign_in'
  include_context "very_short_pomo_length"
  include_context "workspace"

  scenario 'successfully work through, short rest and get back to workplace' do
    todo_task.start_button.click

    home_page.wait_until_timer_form_visible
    timer_form.wait_until_short_rest_button_visible
    timer_form.short_rest_button.click

    timer_form.wait_until_return_to_workspace_button_visible
    timer_form.return_to_workspace_button.click

    expect_for_timer_form_to_be_invisible
  end

  scenario 'successfully work through, long rest and get back to workplace' do
    todo_task.start_button.click

    home_page.wait_until_timer_form_visible
    timer_form.wait_until_long_rest_button_visible
    timer_form.long_rest_button.click

    timer_form.wait_until_return_to_workspace_button_visible
    timer_form.return_to_workspace_button.click

    expect_for_timer_form_to_be_invisible
  end

  scenario "successfully start and cancel a pomodoro" do
    before_start_spent_pomodoros = todo_task.spent.value
    todo_task.start_button.click

    home_page.wait_until_timer_form_visible
    timer_form.wait_until_cancel_button_visible
    expect_rest_buttons_not_to_be_present
    timer_form.cancel_button.click

    expect_for_timer_form_to_be_invisible
    expect(todo_task.spent.value).to eq before_start_spent_pomodoros
  end

  scenario "successfully start, rest and cancel a pomodoro" do
    before_start_spent_pomodoros = todo_task.spent.value
    todo_task.start_button.click

    home_page.wait_until_timer_form_visible
    timer_form.wait_until_short_rest_button_visible
    timer_form.short_rest_button.click

    expect(timer_form.has_cancel_button?).to eq true
    expect_rest_buttons_not_to_be_present
    timer_form.cancel_button.click

    expect_for_timer_form_to_be_invisible
    expect(todo_task.spent.value).to eq before_start_spent_pomodoros
  end
end

feature 'Streaks', js: true do
  include_context 'sign_up_and_sign_in'
  include_context "very_short_pomo_length"
  include_context "workspace"

  scenario "successfully start a streak on work part end" do
    start_todo_task
    wait_for_ajax

    expect(home_page.totals.streak.text).to eq "0"

    start_short_rest
    wait_for_ajax

    expect(home_page.totals.streak.text).to eq "1"
  end

  scenario "successfully strike a streak with 2 pomodoros" do
    Timecop.freeze(now) do
      go_through_pomodoro_and_short_rest
      Timecop.freeze(now + 1.second) do
        go_through_pomodoro_and_short_rest
      end
    end

    wait_for_ajax
    expect(home_page.totals.streak.text).to eq "2"
  end

  scenario "successfully reset streak after idling after short rest" do
    go_through_pomodoro_and_short_rest
    wait_for_ajax

    Timecop.freeze(Time.current + 6.minutes) do
      start_todo_task
      wait_for_ajax

      expect(home_page.totals.streak.text).to eq "0"
    end
  end

  scenario "call it a day resets streak" do
    current_user.update! streak: 2

    home_page.call_it_a_day.click
    wait_for_ajax

    expect(home_page.totals.streak.text).to eq "0"
  end

  private

  def go_through_pomodoro_and_short_rest
    start_todo_task
    start_short_rest
    return_to_workspace

    expect_for_timer_form_to_be_invisible
  end

  def start_todo_task
    todo_task.start_button.click
  end

  def start_short_rest
    home_page.wait_until_timer_form_visible
    timer_form.wait_until_short_rest_button_visible
    timer_form.short_rest_button.click
  end

  def return_to_workspace
    timer_form.wait_until_return_to_workspace_button_visible
    timer_form.return_to_workspace_button.click
  end
end

# TODO
feature 'add a task to AI via timer modal' do
end

def ai_task
  home_page.ai_list.tasks.first
end

def todo_task
  home_page.todo_list.tasks.first
end

def expect_for_timer_form_to_be_invisible
  expect(page).to have_selector("#timerModal", visible: false)
end

def expect_rest_buttons_not_to_be_present
  expect(timer_form.has_no_short_rest_button?).to eq true
  expect(timer_form.has_no_long_rest_button?).to eq true
end
