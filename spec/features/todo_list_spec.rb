require 'rails_helper'
require 'home_page'

describe 'TODO list:', :js => true do
  let(:home_page) { Home.new }
  let(:tasks) { home_page.todo_list.tasks }
  def task
    home_page.todo_list.tasks.first
  end

  include_context 'sign_up_and_sign_in'

  # TODO: color must change when it's checked.
  describe 'done checkbox' do
    it 'is off by default' do
      expect(task.done.checked?).to eq false
    end

    it 'it turned on when clicked' do
      task.done.click
      expect(task.done.checked?).to eq true
    end
  end

  describe 'recurrent checkbox' do
    it 'is not shown for not-done tasks' do
      expect(task.done.checked?).to eq false
      expect(task.root_element.has_no_css? '.recurrent').to eq true
    end

    it 'is shown for done tasks' do
      task.done.click
      expect(task.done.checked?).to eq true
      expect(task.root_element.has_css? '.recurrent').to eq true
    end
  end

  # TODO: check that it can be edited.
  describe 'description' do
    it "isn't done by default" do
      expect(task.description).to have_no_css('.taskDoneName')
    end

    it 'becomes done when "done" checkbox is clicked' do
      task.done.click
      expect(task.description[:class].include?('taskDoneName')).to eq true
    end
  end

  describe 'spent' do
    it 'is zero by default' do
      expect(task.spent.text).to eq 'spent 0'
    end
  end

  describe 'buttons:' do
    [:start, :move, :remove].each { |button|
      context button do
	it 'is enabled by default' do
	  expect(task.start_button[:disabled]).to eq "false"
	end

	it 'gets disabled when task is done' do
	  task.done.click
	  expect(task.start_button[:disabled]).to eq 'true'
	end
      end
    }
  end

  describe 'move button' do
    it 'moves task to AI' do
      task.move_button.click
      expect(tasks).to be_empty
      moved_task = home_page.ai_list.tasks.find { |t|
	t.description.text == 'buy milk'
      }
      expect(moved_task).not_to be_nil
    end
  end

  describe 'remove button' do
    it 'removes task' do
      task.remove_button.click
      expect(tasks).to be_empty
    end
  end
end
