Workspace = Oxheart.Workspace

describe 'Workspace', ->
  it ".buildPageTitle", ->
    Oxheart.appName = "AppName"
    title = Workspace.buildPageTitle("icon", "message")
    expect(title).toBe("icon AppName: message")
