# Workaround Capybara Selenium not firing onChange on empty string.
class Capybara::Selenium::Node
  alias :_orig_set :set

  def set value, **options
    native.send_keys :backspace if value.empty?
    _orig_set value, options
  end
end
