class SignUp < SitePrism::Page
  set_url '/users/sign_up'

  element :email, '#user_email'
  element :password, '#user_password'
  element :password_confirmation, '#user_password_confirmation'
  element :submit_button, 'input[type="submit"]'
end
