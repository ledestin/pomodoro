shared_examples "float settings value" do |setting_name|
  specify "#{setting_name} is valid as a float" do
    settings.send("#{setting_name}=", 0.2)
    expect(user).to be_valid
  end

  specify "#{setting_name} isn't valid as a non-numeric string" do
    settings.send("#{setting_name}=", "oo")
    expect(user).to be_invalid
    expect(user.errors).to have_key(:"setting_objects.#{setting_name}")
  end
end
