require 'sign_in_page'
require 'sign_up_page'
require 'home_page'

module FeatureHelpers
  RSpec.shared_context 'sign_up_and_sign_in' do
    let(:home_page) { Home.new }
    let(:current_user) { create :user }

    before :each do
      sign_up_and_sign_in
      home_page.load
      wait_for_ajax
    end

    after :each do
      logout_permanently
    end
  end

  RSpec.shared_context 'very_short_pomo_length' do
    let(:pomo_length_1_second) { 1.to_f/60 }

    before :each do
      set_pomodoro_lengths_to pomo_length_1_second
      home_page.load
    end
  end

  RSpec.shared_context 'workspace' do
    let(:timer_form) { home_page.timer_form }
    let(:now) { Time.current }
  end

  def sign_up_and_sign_in create_tasks: true
    if create_tasks
      create :task, user: current_user, description: 'second', recurrent: true
      create :task, user: current_user, description: 'first'
      create :task, user: current_user, description: 'buy milk', state: :todo
    end
    login_permanently_as current_user, scope: :user, :run_callbacks => false
  end

  def sign_in user
    (page = SignIn.new).load

    page.email.set user.email
    page.password.set user.password
    page.submit_button.click

    expect(page).to have_text('Signed in successfully');
  end

  def sign_up user
    (page = SignUp.new).load

    page.email.set user.email
    page.password.set user.password
    page.password_confirmation.set user.password
    page.submit_button.click
    expect(page).to have_text('A message with a confirmation link')

    confirm_user grab_url_from_confirmation_email
  end

  private

  def set_pomodoro_lengths_to(minutes)
    current_user.settings(:pomodoro).update_attributes! work_length: minutes,
      short_rest_length: minutes, long_rest_length: minutes
  end

  def confirm_user url
    visit url
    expect(current_path).to eq '/users/sign_in'
    expect(page).to have_text 'Your account was successfully confirmed'
  end

  def grab_url_from_confirmation_email
    email_body = ActionMailer::Base.deliveries.last.body.raw_source
    regex = %r!a href=".+(/users/[^"]+)"!
    expect(email_body).to match regex
    email_body =~ regex
    $1
  end
end
