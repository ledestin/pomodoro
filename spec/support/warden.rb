module Warden
  module Test
    module Helpers

      def login_permanently_as(user, opts = {})
        Warden::Manager.on_request do |proxy|
          opts[:event] || :authentication
          proxy.set_user(user, opts)
        end
      end

      def logout_permanently
	Warden::Manager.clear_request_callbacks
	logout :user
      end

    end
  end

  module Hooks
    def clear_request_callbacks
      _on_request.clear
    end
  end
end
