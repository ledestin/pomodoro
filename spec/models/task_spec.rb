require 'rails_helper'

RSpec.describe Task, :type => :model do
  let(:user) { create :user }

  def build_task **opts
    build :task, user: user, **opts
  end

  def create_task state = :ai
    opts = {}
    if state == :archive
      opts.merge! spent: 1, done: true
    end

    create :task, user: user, description: 'buy tea', state: state, **opts
  end

  def expect_error_message_to_exist task, field
    expect(task.errors.messages[field]).not_to be_nil
  end

  describe 'description' do
    it "can't be nil" do
      task = build_task description: nil
      expect(task.valid?).to eq false
      expect_error_message_to_exist task, :description
    end

    it "can't be empty" do
      task = build_task description: ''
      expect(task.valid?).to eq false
      expect_error_message_to_exist task, :description
    end
  end

  describe 'uniqueness:' do
    context "can't have task with the same description" do
      [:ai, :todo].each do |state|
	it "and state #{state}" do
	  create_task state
	  expect { create_task state }.to \
            raise_error ActiveRecord::RecordInvalid
	end
      end

      it "in both :ai and :todo" do
	create_task :ai
	expect { create_task :todo }.to raise_error ActiveRecord::RecordInvalid
      end
    end

    context 'can have task with the same description,' do
      it 'state :archive' do
	state = :archive
	create_task state
	expect { create_task state }.not_to raise_error
      end

      [:ai, :todo].each do |state|
	it "state #{state}, :archive" do
	  create_task :archive
	  expect { create_task state }.not_to raise_error
	end

	it ":archive, state #{state}" do
	  create_task state
	  expect { create_task :archive }.not_to raise_error
	end
      end
    end
  end

  describe 'state transition:' do
    it 'can transition from :ai to :todo' do
      task = create_task :ai
      expect { task.todo! }.not_to raise_error
    end

    it 'can transition from :todo to :ai' do
      task = create_task :todo
      expect { task.ai! }.not_to raise_error
    end

    context "can't transition from :archive" do
      it 'to :ai' do
	task = create_task :archive
	expect { task.ai! }.to raise_error ActiveRecord::RecordInvalid
      end

      it 'to :todo' do
	task = create_task :archive
	expect { task.todo! }.to raise_error ActiveRecord::RecordInvalid
      end
    end
  end

  describe 'state requirements' do
    context 'for ai:' do
      before :each do
	@task = build_task
      end

      it 'length must be >= 1' do
	@task.length = 0
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :length
      end

      it 'spent must be 0' do
	@task.spent = 1
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :spent
      end

      it 'done must be false' do
	@task.done = true
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :done
      end
    end

    context 'for todo:' do
      before :each do
	@task = build_task state: :todo
      end

      it 'length must be >= 1' do
	@task.length = 0
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :length
      end

      it "spent can't be < 0" do
	@task.spent = -1
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :spent
      end

      it 'spent can be > 0' do
	@task.spent = 1
	expect(@task.valid?).to eq true
      end
    end

    context 'for archive:' do
      before :each do
	@task = build_task state: :archive, done: true
      end

      it 'length must be >= 1' do
	@task.length = 0
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :length
      end

      it "spent can be 0" do
	@task.spent = 0
	expect(@task.valid?).to eq true
      end

      it 'spent can be > 0' do
	@task.spent = 1
	expect(@task.valid?).to eq true
      end

      it "recurrent can't be true" do
	@task.recurrent = true
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :recurrent
      end

      it "done can't be false" do
	@task.done = false
	expect(@task.valid?).to eq false
	expect_error_message_to_exist @task, :done
      end
    end
  end

  describe 'recurrent' do
    it 'is false by default' do
      expect(create_task.recurrent).to eq false
    end

    it "can't be true for state :archive" do
      task = build_task state: :archive, recurrent: true
      expect(task.valid?).to eq false
      expect_error_message_to_exist task, :recurrent
    end
  end

  describe 'length' do
    it 'is 1 by default' do
      expect(create_task.length).to eq 1
    end

    it 'is never 0' do
      task = build_task length: 0
      expect(task.invalid?).to eq true
      expect_error_message_to_exist task, :length
    end
  end

  describe 'spent' do
    it 'is 0 by default' do
      expect(create_task.spent).to eq 0
    end
  end

  describe 'done' do
    it 'is false by default' do
      expect(create_task.done).to eq false
    end
  end
end
