require 'rails_helper'

describe Settings do
  let(:user) { build :user }
  let(:settings) { user.settings(:pomodoro) }

  specify "default settings are valid" do
    expect(user.valid?).to eq true
  end

  include_examples "float settings value", :work_length
  include_examples "float settings value", :short_rest_length
  include_examples "float settings value", :long_rest_length
end
