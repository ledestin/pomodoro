require 'rails_helper'

RSpec.describe MyConfig, :type => :model do
  it 'work_length is an integer' do
    expect(subject.work_length).to be_kind_of(Integer)
  end

  it 'rest_length is an integer' do
    expect(subject.rest_length).to be_kind_of(Integer)
  end
end
