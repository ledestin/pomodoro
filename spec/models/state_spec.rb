require 'rails_helper'

RSpec.describe State, :type => :model do
  let(:user) { create :user }

  def create_task **opts
    create :task, user: user, **opts
  end

  let(:state) { State.new user }

  describe 'a new task' do
    it 'gets placed into AI' do
      task = create_task
      expect(state.ai.first).to eq task
    end
  end

  describe 'task moved from AI to TODO' do
    it 'is accessible via #todo' do
      task = create_task
      task.todo!
      expect(state.todo.first).to eq task
    end
  end

  describe 'at the end of the day,' do
    it 'undone TODO tasks move to AI' do
      task = create_task state: :todo
      state.call_it_a_day!
      expect(state.ai.first).to eq task
      expect(state.todo).to be_empty
    end

    it 'done TODO tasks move to Archive' do
      task = create_task state: :todo, done: true, spent: 1
      state.call_it_a_day!
      expect(state.archive.first).to eq task
    end

    context 'recurrent done TODO task' do
      before :each do
        @task = create_task state: :todo, recurrent: true, done: true, spent: 1
	state.call_it_a_day!
      end

      it 'is cloned to AI as it is recurrent' do
	old_attrs, new_attrs = @task.attributes, state.ai.first.attributes
	[old_attrs, new_attrs].each { |attrs|
	  %w(id done state spent created_at updated_at).each { |attr|
	    attrs.delete attr
	  }
	}
	expect(old_attrs).to eq new_attrs
      end

      it 'is moved to archive' do
	@task.reload
	expect(@task.archive?).to eq true
      end
    end
  end
end
