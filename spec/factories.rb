FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.com" }
    password { 'rei2aiTh' }
  end

  factory :task do
    user
    sequence(:description) { |n| "buy foo#{n}" }
  end
end
