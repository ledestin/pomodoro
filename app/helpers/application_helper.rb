module ApplicationHelper
  def menu_item_active_inactive_class(active_for_this_controller_name)
    controller_name == active_for_this_controller_name ?
      raw("class='active'") : ''

  end
end
