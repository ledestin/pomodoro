class WorkspaceController < ApplicationController
  respond_to :json

  before_action :authenticate_user!

  def index
  end

  def call_it_a_day
    State.new(current_user).call_it_a_day!
    redirect_to workspace_path
  end

  def streak
    respond_with current_user.streak
  end

  def work_part_started
    current_user.with_lock do
      current_user.work_part_started
      current_user.save!
    end

    respond_with current_user.streak
  end

  def work_part_ended
    current_user.with_lock do
      current_user.work_part_ended
      current_user.save!
    end

    respond_with current_user.streak
  end
end
