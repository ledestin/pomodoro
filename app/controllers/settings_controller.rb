class SettingsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @settings = current_user.settings :pomodoro

    respond_to do |format|
      format.json { render json: @settings }
      format.html
    end
  end

  def update
    @settings = current_user.settings :pomodoro

    if @settings.update_attributes settings_params
      flash[:success] = "Successfully saved settings"
      redirect_to workspace_path
    end
  end

  private

  def settings_params
    params.require(:settings)
      .permit(:work_length, :short_rest_length, :long_rest_length)
  end
end
