class TasksController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def create
    task = Task.new task_params
    task.user = current_user
    if task.save
      respond_with task
    else
      render json: { errors: task.errors.full_messages },
	status: :unprocessable_entity
    end
  end

  def destroy
    unless task = fetch_task
      render_no_such_task
    else
      task.destroy
      head :no_content
    end
  end

  def index
    unless %w(ai todo).include? params[:state]
      render nothing: true, status: :bad_request
      return
    end

    @tasks = current_user.tasks.order(:description).send(params[:state])
    respond_with @tasks
  end

  def update
    unless task = fetch_task
      render_no_such_task
      return
    end

    if task.update_attributes task_params
      respond_with
    else
      render json: { errors: task.errors.full_messages },
	status: :unprocessable_entity
    end
  end

  private

  def fetch_task
    current_user.tasks.find_by(id: params[:id])
  end

  def render_no_such_task
    render json: { errors: ['No such task'] }, status: :unprocessable_entity
  end

  def task_params
    params.require :task
    params[:task].permit(:description, :length, :recurrent, :spent, :done,
      :state)
  end
end
