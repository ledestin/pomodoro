class Task < ActiveRecord::Base
  belongs_to :user
  enum state: [:ai, :todo, :archive]

  scope :done, -> { where done: true }
  scope :undone, -> { where done: false }
  scope :recurrent, -> { where recurrent: true }

  validates_presence_of :description
  validates_uniqueness_of :description, scope: :user_id,
    conditions: -> { where.not(state: self.states[:archive]) },
    unless: proc { |r| r.archive? }

  validates_each :state do |record, attr, value|
    original_value = record.changed_attributes[attr]
    record.errors.add(attr, "can't be changed from :archive") \
      if original_value == 'archive'

    case value
      when 'ai' then validate_ai record, attr
      when 'todo' then validate_todo record, attr
      when 'archive' then validate_archive record, attr
    end
  end

  validates_each :recurrent do |record, attr, value|
    record.errors.add(attr, "can't have recurrent task in :archive") \
      if record.archive? && record.recurrent
  end
  validates :recurrent, :inclusion => {:in => [true, false]}

  validates_presence_of :length
  validates_numericality_of :length, only_integer: true,
    greater_than_or_equal_to: 1

  validates_numericality_of :spent, only_integer: true, min: 0
  validates_numericality_of :spent, greater_than_or_equal_to: 0,
    if: proc { |r| r.archive? }

  validates :done, :inclusion => {:in => [true, false]}

  def self.validate_ai record, attr
    record.errors.add(:spent, 'must be 0') unless record.spent.zero?
    record.errors.add(:done, 'must be false') unless record.done == false
  end

  def self.validate_archive record, attr
    record.errors.add(:done, 'must be true') unless record.done
  end

  def self.validate_todo record, attr
    record.errors.add(:spent, 'must be >= 0') unless record.spent >= 0
  end

  def ai! save: true
    self.attributes = { state: :ai, spent: 0, done: false }
    save! if save
  end

  def archive! save: true
    self.attributes = { state: :archive, done: true, recurrent: false }
    save! if save
  end
end
