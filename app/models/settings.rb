class Settings < RailsSettings::SettingObject
  validates_numericality_of :work_length
  validates_numericality_of :short_rest_length
  validates_numericality_of :long_rest_length

  def as_json(options=nil)
    { pomodoro:
      { work_length: work_length, short_rest_length: short_rest_length,
        long_rest_length: long_rest_length }
    }
  end
end
