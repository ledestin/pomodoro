class State
  delegate :tasks, to: :@user
  delegate :ai, :archive, :todo, to: :tasks

  def initialize user
    @user = user
  end

  def call_it_a_day!
    Task.transaction {
      move_undone_to_ai

      cloned_to_ai = clone_done_recurrent_to_ai
      move_done_to_archive
      cloned_to_ai.each &:save!

      @user.reset_streak
    }
  end

  private

  def clone_done_recurrent_to_ai
    todo.done.recurrent.map { |task|
      ai_task = task.dup
      ai_task.ai! save: false
      ai_task
    }
  end

  def move_done_to_archive
    todo.done.each &:archive!
  end

  def move_undone_to_ai
    todo.undone.each &:ai!
  end
end
