class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tasks, dependent: :delete_all

  has_settings class_name: 'Settings' do |settings|
    settings.key :pomodoro,
      defaults: { work_length: 25, short_rest_length: 5, long_rest_length: 30 }
  end

  def work_part_started
    self.work_part_started_at = Time.current
    self.streak = 0 if long_gap?
  end

  def work_part_ended
    self.work_part_ended_at = Time.current
    self.streak += 1
  end

  def reset_streak
    update! streak: 0
  end

  def config
    @config ||= MyConfig.new
  end

  private

  def long_gap?
    return false unless work_part_started_at && work_part_ended_at

    work_part_started_at - work_part_ended_at > short_rest_length + rest_padding
  end

  def short_rest_length
    settings(:pomodoro).short_rest_length.to_f * 60
  end

  def rest_padding
    5.minutes
  end

  def work_length
    settings(:pomodoro).work_length.to_f * 60
  end
end
