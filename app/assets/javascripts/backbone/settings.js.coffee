class Oxheart.Models.Settings extends Backbone.Model
  urlRoot: '/settings'

  parse: (response) ->
    pomodoro_settings = {}
    for k, v of response.pomodoro
      pomodoro_settings[lodash.camelCase(k)] = parseFloat(v)
    pomodoro_settings
