class Oxheart.Models.Task extends Backbone.Model
  paramRoot: 'task'
  urlRoot: '/tasks'

  defaults: { length: 1 }

  initialize: ->
    @on 'change', (task) ->
      @save()

  onSaveError: (model, response, options) ->
    @errors = $.parseJSON(response.responseText).errors

  onSaveSuccess: ->
    @errors = null

  save: (attrs, options = {}) ->
    mergedOptions = $.extend({}, options)

    # Ensure passed callbacks and this model's callbacks get called on error and
    # success.
    error = options.error
    mergedOptions.error = (model, response, options) =>
      @onSaveError(model, response, options)
      error(model, response, options) if error

    success = options.success
    mergedOptions.success = (model, response, options) =>
      @onSaveSuccess(model, response, options)
      success(model, response, options) if success

    super attrs, mergedOptions

  started: -> @get('spent') > 0

class Oxheart.Collections.TasksCollection extends Backbone.Collection
  model: Oxheart.Models.Task

  url: ->
    "/tasks?state=#{@state}"

  selected: ->
    @filter (task) ->
      task.get 'selected'

  totalLength: ->
    @total 'length'

  totalSpent: ->
    @total 'spent'

  # private

  total: (field) ->
    @reduce((memo, task) ->
      memo += task.get field
    , 0)
