class Timer
  hide: ->
    $('#timerModal').modal('hide')

  onWorkStart: ->
  onWorkEnd: ->
  onRestStart: ->
  onRestEnd: ->

  restEnded: =>
    @cancelButton().hide()
    @timerAudio().play()
    @returnButton().show()
    @onRestEnd()

  shortRestStart: =>
    @restStart('Rest time', @shortRestLength)

  longRestStart: =>
    @restStart('Rest time (long)', @longRestLength)

  show: ->
    $('#timerModal').modal('show')

  start: (@taskDescription, @workLength, @shortRestLength,
        @longRestLength) =>
    @cancelButton().show()
    @hideRestButtons()
    @returnButton().hide()
    @workStart()

  stop: ->
    $('#timer').countdown('destroy')
    @timerAudio().pause()
    @timerAudio().currentTime = 0

  workEnded: =>
    @timerAudio().play()
    @showRestButtons()
    @onWorkEnd()

  workStart: =>
    @setTitle 'Work time'
    @_start 'Until work ends', Util.futureDate(@workLength), @workEnded
    @onWorkStart()

  # private

  restStart: (title, length) =>
    @stop()
    @hideRestButtons()
    @setTitle title
    @_start 'Until rest ends', Util.futureDate(length), @restEnded
    @onRestStart()

  hideRestButtons: =>
    @restButton().hide()
    @longRestButton().hide()

  showRestButtons: =>
    @restButton().show()
    @longRestButton().show()

  cancelButton: ->
    $('#timerCancelButton')

  restButton: ->
    $('#timerRestButton')

  longRestButton: ->
    $('#timerLongRestButton')

  returnButton: ->
    $('#timerReturnButton')

  setTitle: (title) ->
    $('#timerTitle').text "#{title} - #{@taskDescription}"

  timerAudio: ->
    $('#timerAudio')[0]

  _start: (description, delay, expireCallback) ->
    $('#timer').countdown until: delay, format: 'MS', \
      onExpiry: expireCallback, \
      description: description, \
      layout: '<div class="timeLeft">{mnn}{sep}{snn}</div> <div class="centeredText">{desc}</div>'

window.Oxheart.Timer = Timer
