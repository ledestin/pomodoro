// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require es6-shim
//= require react
//= require underscore
//= require backbone
//= require backbone_rails_sync
//= require backbone_datalink
//= require backbone/oxheart
//= require backbone-react-component
//= require elucidata-react-coffee
//= require uri
//= require lodash/dist/lodash
//= require ./index.js

//= require jquery-titlealert/jquery.titlealert.js

// Ensure that $.noRequestAnimationFrame is set before Countdown plugin is
// loaded. If not, onExpiry won't be called on time.
//= require jquery.countdown/jquery.plugin.js
//= require jquery.countdown/jquery.countdown.js
