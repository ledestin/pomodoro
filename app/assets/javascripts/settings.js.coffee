class Settings
  @workLength: ->
    settings.get("workLength")

  @shortRestLength: ->
    settings.get("shortRestLength")

  @longRestLength: ->
    settings.get("longRestLength")

window.Oxheart.Settings = Settings
