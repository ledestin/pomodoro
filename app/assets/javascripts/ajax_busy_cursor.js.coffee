$(document).ajaxStart(->
  $('body').addClass('busy')
).ajaxComplete(->
  $('body').removeClass('busy')
)
