# Generic text input component.
# Properties: onEnterKey, onEscapeKey
# Methods:
# val() returns current value.
class TextInputJ extends React.Component
  mixins: [ConsumePropsMixin]

  NOOP = ->

  propTypes: {
    onEnterKey: React.PropTypes.func,
    onEscapeKey: React.PropTypes.func
  }

  componentWillReceiveProps: (nextProps) ->
    @setState value: nextProps.value

  consumedProps: ->
    ['type', 'ref', 'className', 'value', 'onKeyDown', 'onChange', 'onEnterKey',
      'onEscapeKey']

  getDefaultProps: ->
    value: '', onEnterKey: NOOP, onEscapeKey: NOOP

  getInitialState: ->
    value: @props.value

  handleInput: (event) ->
    @setState value: event.target.value

  handleKeyDown: (event) ->
    switch event.keyCode
      when 13
        @props.onEnterKey()
      when 27
        @reset()
        @props.onEscapeKey()

  render: ->
    (
      `<input {...this.notConsumedProps()} type="text" ref="Input"
         className={this.props.className}
         value={this.state.value}
         onKeyDown={this.handleKeyDown}
         onChange={this.handleInput} />`
    )

  reset: ->
    @setState @getInitialState()

  val: ->
    @state.value

window.TextInputJ = TextInputJ
window.TextInput = TextInputJ.toComponent()
