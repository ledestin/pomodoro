# How to use:
# 1. Define consumedProps() to return array of property names, consumed by the
#    class (optionally call super).
# 2. Call notConsumedProps() to get list of properties not consumed by the class
#    or its ancestors (if you caled super).
ConsumePropsMixin = {
  notConsumedProps: ->
    _.omit(@props, @consumedProps())
}

window.ConsumePropsMixin = ConsumePropsMixin
