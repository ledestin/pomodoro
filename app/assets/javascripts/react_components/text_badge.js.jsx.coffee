class TextBadge extends React.Component
  propTypes: {
    label: React.PropTypes.string
    badgeClass: React.PropTypes.string
  }

  mixins: [ConsumePropsMixin]

  consumedProps: ->
    ['label', 'badge', 'badgeClass']

  badgeClass: ->
    "badge #{@props.badgeClass}"

  render: ->
    `<label className="label label-default" {...this.notConsumedProps()}>
       {this.props.label}&nbsp;
       <span className={this.badgeClass()}>
         {this.props.badge}
       </span>
     </label>`

window.TextBadge = TextBadge.toComponent()
