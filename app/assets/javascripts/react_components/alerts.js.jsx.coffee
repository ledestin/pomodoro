# Bootstrap danger alerts (react component).
#
# Properties:
# @messages - an array of strings, representing error messages.
class Alerts extends React.Component
  propTypes: {
    messages: React.PropTypes.array
  }

  render: ->
    messages = @props.messages || []
    return `<span></span>` if messages.length == 0

    messages = messages.map((message) ->
      `<div key={message}>{message}</div>`)
    `<div id={this.props.id} className="alert alert-danger">{messages}</div>`

window.Alerts = Alerts.toComponent()
