# onSubmit - only called after enter is pressed within the input, and only if
#            the entered value is different from the passed props value.
# value    - default value for showing and prefilling input.
class EditableTextJ extends React.Component
  mixins: [ConsumePropsMixin]

  propTypes: {
    onSubmit: React.PropTypes.func
    value: React.PropTypes.string
  }

  consumedProps: ->
    ['onSubmit', 'value', 'onBlur', 'onEnterKey', 'onEscapeKey']

  editContent: ->
    (
      `<TextInput {...this.notConsumedProps()} ref="Input"
         value={this.props.value} onBlur={this.handleCancel}
         onEnterKey={this.handleSubmit}
         onEscapeKey={this.handleCancel} />`
    )

  editMode: ->
    @setState { editMode: true }, @focusInput

  focusInput: ->
    ReactDOM.findDOMNode(@refs.Input).select()

  getInitialState: ->
    editMode: false

  getInput: ->
    @refs.Input.val()

  handleCancel: ->
    @viewMode()

  handleSubmit: ->
    newVal = @getInput()
    if newVal != @props.value
      @props.onSubmit newVal
    @viewMode()

  render: ->
    if @state.editMode then @editContent() else @viewContent()

  viewContent: ->
    (
      `<span {...this.notConsumedProps()} onClick={this.editMode}>
         {this.props.value}
       </span>`
    )

  viewMode: ->
    @setState { editMode: false }

window.EditableTextJ = EditableTextJ
window.EditableText = EditableTextJ.toComponent()
