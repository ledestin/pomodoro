# Numeric input field.
#
# Features:
# * Only allows numbers in.
#
# Properties:
# * onEnterKey, onEscapeKey, min, max.
#
# Methods:
# * val() returns current value.
class IntegerInput extends TextInputJ
  consumedProps: ->
    super.concat ['min', 'max']

  handleInput: (event) ->
    input = event.target.value
    # NOTE: this is to workaround case when React doesn't trigger onChange when
    # Selenium clears input. If a space is passed, Selenium uses sendKeys and
    # onChange is triggered.
    input = $.trim input
    if @validInput input
      @setState value: @numberOrEmpty(input)

  numberOrEmpty: (str) ->
    if str then Number(str) else str

  # Allow empty input and valid numbers.
  validInput: (input) ->
    !input || @inRange(input)

  inRange: (string) ->
    number = Number(string)
    return if isNaN number
    return if @props.min? && number < @props.min
    return if @props.max? && number > @props.max
    true

window.IntegerInput = IntegerInput.toComponent()
