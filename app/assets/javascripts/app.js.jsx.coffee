class App extends React.Component
  getInitialState: ->
    { streak: 0 }

  componentDidMount: ->
    @fetchStreak()
    Oxheart.Workspace.onWorkStartCallback = @notifyBackendThatWorkStarted
    Oxheart.Workspace.onWorkEndCallback = @notifyBackendThatWorkEnded

  render: ->
    `<div>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
          <div className="well">
            <div id="addForm">
              <AddForm tasks={ai_tasks} />
            </div>
          </div>
        </div>

        <div className="col-xs-12 col-lg-6">
          <a className="btn btn-warning btn-lg" href="/workspace/call_it_a_day">Call it a day!</a>
          <TotalsR streak={this.state.streak} collection={todo_tasks} />
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12 col-lg-6">
          <div id="activityInventory">
            <AiListR name="Activity Inventory"
               moveButtonClass="glyphicon-arrow-right"
               collection={ai_tasks} todo_tasks={todo_tasks} />
          </div>
        </div>
        <div className="col-xs-12 col-lg-6">
          <div id="todayTODO">
            <TodoListR name="Today TODO" collection={todo_tasks} ai_tasks={ai_tasks}
               moveButtonClass="glyphicon-arrow-left" />
          </div>
        </div>
      </div>
    </div>`

  fetchStreak: ->
    @runAjaxAndSetStreak("/streak")

  notifyBackendThatWorkStarted: ->
    @runAjaxAndSetStreak("/work_part_started")

  notifyBackendThatWorkEnded: ->
    @runAjaxAndSetStreak("/work_part_ended")

  runAjaxAndSetStreak: (url) ->
    $.ajax(url).success((response) =>
      @setState({ streak: response})
    )

window.AppR = App.toComponent()

$(document).on('turbolinks:load', ->
  return if $('#root').length == 0

  ReactDOM.render(
    `<AppR />`,
    document.querySelector('#root')
  )
)
