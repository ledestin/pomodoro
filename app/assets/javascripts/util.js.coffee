class Util
  @futureDate: (minutes) ->
    new Date(new Date().getTime() + minutes*60000)

  @isCurrentPath: (path) =>
    URI(window.location.href).path() == path

window.Util = Util
