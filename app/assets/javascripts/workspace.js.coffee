# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

Settings = window.Oxheart.Settings
timer = new window.Oxheart.Timer

class Workspace
  @init: ->
    @onFinishCallbacks = []

  @initTimerCallbacks: ->
    timer.onWorkStart = =>
      @setPageTitle '▶',  'work time'

    timer.onWorkEnd = =>
      @setPageTitle '▶', 'work ended'

    timer.onRestStart = =>
      @setPageTitle '◼', 'rest time'

    timer.onRestEnd = =>
      @setPageTitle '◼', 'rest ended'

  @addOnFinishCallback = (callback) ->
    @onFinishCallbacks.push(callback)

  @oneOffFinishCallback = ->

  @onWorkStartCallback: ->

  @onWorkEndCallback = ->

  @returnToWorkspace: ->
    @closeTimer()
    @onFinishCallbacks.forEach((callback) ->
      callback()
    )
    @oneOffFinishCallback()
    @setPageTitle()

  @startPomodoro: (taskDescription, onFinishCallback) ->
    @initTimerCallbacks()
    @onWorkStartCallback()
    @oneOffFinishCallback = onFinishCallback
    timer.show()
    timer.start taskDescription, Settings.workLength(),
      Settings.shortRestLength(), Settings.longRestLength()

  @cancelPomodoro: =>
    @closeTimer()
    @setPageTitle()

  @closeTimer: ->
    timer.hide()
    timer.stop()

  @buildPageTitle: (icon, text) ->
    newTitle = ''
    newTitle += "#{icon} " if icon
    newTitle += window.Oxheart.appName
    newTitle += ": #{text}" if text
    newTitle

  @setPageTitle: (icon, text) ->
    document.title = @buildPageTitle(icon, text)

  @startRest: ->
    timer.shortRestStart()
    @onWorkEndCallback()

  @startLongRest: ->
    timer.longRestStart()
    @onWorkEndCallback()

$.noRequestAnimationFrame = true
window.Oxheart.Workspace = Workspace

# Task collections.
ai_tasks = new Oxheart.Collections.TasksCollection()
ai_tasks.state = 'ai'
todo_tasks = new Oxheart.Collections.TasksCollection()
todo_tasks.state = 'todo'
settings = new Oxheart.Models.Settings()

$(document).on('turbolinks:load', ->
  return unless Util.isCurrentPath '/workspace'

  Workspace.init()

  ai_tasks.fetch()
  todo_tasks.fetch()
  settings.fetch()
)

window.ai_tasks = ai_tasks
window.todo_tasks = todo_tasks
window.settings = settings
