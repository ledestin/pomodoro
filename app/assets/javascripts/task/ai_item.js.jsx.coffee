class Task.Ai.Item extends Task.ListItem
  leftContent: ->
    @recurrentCheckbox()

  rightContent: ->
    superContent = super()
    `<span>
       <Task.EditableLength value={this.state.model.length}
         onSubmit={this.set.bind(this, 'length')} />
       &nbsp;
       {superContent}
     </span>`

window.Task.Ai.Item = Task.Ai.Item.toComponent()
