# Task length input field. The only reason for this component is to hardcode
# "class", "min" and "max" for the input.
#
# Features:
# * Only allows integers.
# * Min is 1, max is 99.
#
# Properties:
# * id, value, onEnterKey, onEscapeKey.
#
# Methods:
# * val() contains current value.
class Task.LengthInput extends React.Component
  propTypes: {
    onBlur: React.PropTypes.func
    onEnterKey: React.PropTypes.func
    onEscapeKey: React.PropTypes.func
    value: React.PropTypes.number
  }

  render: ->
    (
      `<IntegerInput ref="Input" id={this.props.id}
         name="task_length"
         className="task_length"
         min="1" max="99"
         value={this.props.value}
         onBlur={this.props.onBlur}
         onEnterKey={this.props.onEnterKey}
         onEscapeKey={this.props.onEscapeKey} />`
    )

  val: ->
    @refs.Input.val()

window.Task.LengthInput = Task.LengthInput.toComponent()
