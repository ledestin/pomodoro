class Task.EditableLength extends EditableTextJ
  propTypes: {
    disabled: React.PropTypes.bool
    onSubmit: React.PropTypes.func
    value: React.PropTypes.number
  }

  editContent: ->
    (
      `<Task.LengthInput ref="Input" value={this.props.value}
         onBlur={this.handleCancel}
         onEnterKey={this.handleSubmit}
         onEscapeKey={this.handleCancel} />`
    )

  viewContent: ->
    `<TextBadge label="length" badge={this.props.value}
       disabled={this.props.disabled} badgeClass="lengthBadge"
       name="task_length" onClick={this.editMode} />`

window.Task.EditableLength = Task.EditableLength.toComponent()
