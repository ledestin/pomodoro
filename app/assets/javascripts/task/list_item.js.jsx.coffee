class ListItem extends React.Component
  mixins: [Backbone.React.Component.mixin]

  descriptionContent: ->
    `<EditableText name="task_description" value={this.state.model.description}
      onSubmit={this.set.bind(this, 'description')} />`

  getInitialState: ->
    errors: null

  leftContent: ->

  moveButtonClass: ->
    "btn btn-success glyphicon #{@props.moveButtonClass}"

  moveButtonDisabled: -> false

  removeButtonDisabled: -> false

  recurrentCheckbox: ->
    `<span>
      <input type="checkbox" className="recurrent" name="recurrent"
        checked={this.state.model.recurrent}
        onChange={this.toggle.bind(this, 'recurrent')}
      />
    </span>`

  render: ->
    `<li className="taskItem">
      <div className="container-fluid">
        <div className="row">
          <Alerts messages={this.getModel().errors} />
        </div>
        <div className="row">
          <span className="col-xs-12">
            {this.leftContent()}
            &nbsp;
            {this.descriptionContent()}
            <span className="pull-right">
             {this.rightContent()}
            </span>
          </span>
        </div>
      </div>
    </li>`

  rightContent: ->
    `<span>
      <button name="move_button" className={this.moveButtonClass()}
	onClick={this.props.onMove} disabled={this.moveButtonDisabled()} />
      &nbsp;
      <button name="remove_button"
        className="btn btn-warning glyphicon glyphicon-remove"
        onClick={this.props.onRemove}
        disabled={this.removeButtonDisabled()} />
    </span>`

  toggle: (fieldName) ->
    @getModel().set(fieldName, !@state.model[fieldName])

  set: (fieldName, value) ->
    @getModel().set(fieldName, value)

window.Task.ListItem = ListItem
