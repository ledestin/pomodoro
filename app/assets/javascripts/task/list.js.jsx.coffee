class List extends React.Component
  mixins: [Backbone.React.Component.mixin],

  moveTask: ->

  removeTask: (task) ->
    @getCollection().remove task
    task.destroy()

  render: ->
    items = @getCollection().map((task) => @task2Item task)

    `<div className="well">
      <h2 className="taskList">{this.props.name}</h2>
      <ul className="taskList">
        {items}
      </ul>
    </div>`

  task2Item: ->

window.Task.List = List
