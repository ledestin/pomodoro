class TodoList extends Task.List
  moveTask: (task) ->
    task.set 'state', 'ai'
    @getCollection().remove task
    @props.ai_tasks.add task, at: 0

  startPomodoro: (task) ->
    Oxheart.Workspace.startPomodoro task.get('description'), ->
      task.set('spent', task.get('spent') + 1)

  task2Item: (task) ->
    `<Task.Todo.Item key={task.cid} model={task}
       onRemove={this.removeTask.bind(this, task)}
       onMove={this.moveTask.bind(this, task)}
       onPomodoroStart={this.startPomodoro.bind(this, task)}
       moveButtonClass={this.props.moveButtonClass} />`

window.TodoListR = TodoList.toComponent()
