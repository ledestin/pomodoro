// Form to add a new task to a task collection.
//
// Properties:
// @tasks - collection of tasks to add a new task to.
AddForm = React.createClass({
  handleInput: function(e) {
    this.updateTaskFromInput();
    e.preventDefault();
  },

  getInitialState: function() {
    return { task: this.newTask() };
  },

  // 'atf' means 'Add task form'
  render: function() {
    return (
      <div>
	<Alerts id='atf_alerts' messages={this.state.task.errors} />
	<form onSubmit={this.handleInput}>
	  <label name='description'>New activity
	    &nbsp;
	    <TextInput ref="taskDescription" id="atf_description"
	      placeholder="Description"
	      value={this.state.task.get('description')} />
	  </label>
	  &nbsp;
	  <label name='length'>Length
	    &nbsp;
	    <Task.LengthInput ref="taskLength" id="atf_length"
	      value={this.state.task.get('length')} />
	  </label>
	  &nbsp;

	  <button className="btn btn-success" id="atf_submit" type="submit">Add
	  </button>
	</form>
      </div>
    );
  },

  // private
  addTask: function() {
    this.props.tasks.add(this.state.task, { at: 0 });
  },

  getInput: function() {
    var inputEl = ReactDOM.findDOMNode(this.refs.taskDescription);
    var length = this.refs.taskLength.val();
    return { description: $.trim(inputEl.value), length: length };
  },

  newTask: function() {
    var that = this;
    var task = new this.props.tasks.model();
    task.once('error', function() {
      // Make sure alerts show up.
      that.forceUpdate();
    });

    task.once('sync', function() {
      that.addTask();
      that.spawnNewTask();
    });

    return task;
  },

  spawnNewTask: function() {
    this.setState(this.getInitialState());
  },

  updateTaskFromInput: function() {
    var input = this.getInput();
    this.state.task.set({
      description: input.description,
      length: input.length
    });
  }
});

var ready = function() {
  if ($('#timerAddForm').length === 0) return;

  ReactDOM.render(<AddForm tasks={ai_tasks}/>, $('#timerAddForm')[0]);
}
$(document).on('turbolinks:load', ready);
