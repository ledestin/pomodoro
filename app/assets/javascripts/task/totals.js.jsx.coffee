class Totals extends React.Component
  mixins: [Backbone.React.Component.mixin]

  render: ->
    `<div id="todayTODOTotals">
       <h3>
         Today's total
         &nbsp;
         <TextBadge label="length" badge={this.getCollection().totalLength()}
           badgeClass="lengthBadge" />
         &nbsp;
         <TextBadge label="spent" badge={this.getCollection().totalSpent()}
           badgeClass="spentBadge" />
         &nbsp;
         <TextBadge label="streak" badge={this.props.streak}
           badgeClass="streakBadge" />
       </h3>
     </div>`

window.TotalsR = Totals.toComponent()
