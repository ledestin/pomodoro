class AiList extends Task.List
  moveTask: (task) ->
    @getCollection().remove task
    task.set 'state', 'todo'
    @props.todo_tasks.add task, at: 0

  task2Item: (task) ->
    `<Task.Ai.Item key={task.cid} model={task}
      onRemove={this.removeTask.bind(this, task)}
      onMove={this.moveTask.bind(this, task)}
      moveButtonClass={this.props.moveButtonClass} />`

window.AiListR = AiList.toComponent()
