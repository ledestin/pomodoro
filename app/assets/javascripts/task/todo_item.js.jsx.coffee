# Todo item.
#
# Properties:
# @onPomodoroStart - callback to call when 'start' button is pressed.
class Task.Todo.Item extends Task.ListItem
  descriptionContent: ->
    return super unless @state.model.done

    `<span>
       {this.recurrentCheckbox()}
       &nbsp;
       <span name="task_description" className="taskDoneName">
         {this.state.model.description}
       </span>
     </span>`

  leftContent: ->
    `<span>
      <input type="checkbox" className="done" name="done"
        checked={this.state.model.done}
        onChange={this.toggle.bind(this, 'done')} />
    </span>`

  rightContent: ->
    superContent = super()
    `<span>
       <Task.EditableLength value={this.state.model.length}
         disabled={this.state.model.done}
         onSubmit={this.set.bind(this, 'length')} />
       &nbsp;
       <TextBadge name="spent" label="spent" badge={this.state.model.spent}
         disabled={this.state.model.done}
         badgeClass="spentBadge" />
       &nbsp;
       <button name="start" className="btn btn-success"
	 disabled={this.state.model.done}
	 onClick={this.props.onPomodoroStart}>Start</button>
       &nbsp;
       {superContent}
     </span>`

  moveButtonDisabled: ->
    @startedOrDone()

  removeButtonDisabled: ->
    @startedOrDone()

  startedOrDone: ->
    @getModel().started() || @state.model.done

window.Task.Todo.Item = Task.Todo.Item.toComponent()
