Pomodoro
========

[![Build Status](https://semaphoreapp.com/api/v1/projects/0f61957b-1ade-44e7-ae68-5129adce5ce3/300344/badge.png)](https://semaphoreapp.com/ledestin/pomodoro)

# Screenshots

![Work time](doc/Worktime.png)
![Workspace](doc/Workspace.png)
