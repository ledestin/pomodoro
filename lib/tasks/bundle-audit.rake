namespace :bundle do
  desc 'Audits Gemfile.lock for known vulnerabilities'
  task :audit do
    puts "\nbundle-audit:"
    unless system('bundle-audit --update')
      exit 1
    end
  end
end

task :default => 'bundle:audit'
