source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.2.4.4'

gem 'rake', '~> 12.3.2'
gem 'bower-rails', '~> 0.8'

gem 'pg', '>= 0.18', '< 2.0'
gem 'migration_comments'

# Use SCSS for stylesheets
gem 'sassc-rails'
gem 'bootstrap-sass'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '~> 4.1.20'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

gem 'sprockets', '~> 3.7'
gem 'pry-rails'
gem 'puma'
gem 'mini_racer'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :production do
  gem 'exception_notification'
end

group :development, :test do
  gem 'rspec-rails'
  gem 'rb-readline'
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'jasmine-rails'

  gem 'site_prism', '~> 3.0'
  gem 'capybara-screenshot'
end

group :development do
  gem 'bullet'
  gem 'capistrano-rails'
  gem 'foreman'
  gem 'pry-nav'
  gem 'rubocop'
end

group :test do
  #gem 'capybara-slow_finder_errors'
  gem 'database_cleaner'
  gem 'factory_bot_rails', '~> 5.0'
  gem 'webdrivers'
  gem 'rails-controller-testing'
  gem 'timecop'
end

gem 'devise', '~> 4.7.1'
gem 'devise-bootstrap-views'
gem 'slim-rails', '~> 3.2'
gem 'react-rails', '~> 1.11.0'
gem 'responders', '~> 2.0'
gem 'rails-backbone'
gem 'underscore-rails', '~> 1.6'
gem 'font-awesome-rails'
gem 'rails_layout'
gem 'ledermann-rails-settings'
gem 'dynamic_form'

gem 'bundler-audit'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
