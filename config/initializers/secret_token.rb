# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Pomodoro::Application.config.secret_key_base = '1f2ed0e3f831d8922e0759fbf57b1aee5772d7e3900d2b22fb8aa2d6dc94db73238bcf31172d18ad7f83534d0130024997a44a2793cd20b30c6e32b7b1e10f5b'
