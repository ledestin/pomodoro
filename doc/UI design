## Categories

A category groups items like this:

Home
  Take out garbage
  Take a walk
Work
  UI design

## Tags

Tags allow more flexibility than categories, but are more complex. So, we could
select a few tags and all items that have all those tags would be selected and
moved to the top. Not what I want, no. I'd have to group them all the time! This
means, the grouping must be automatic. But how to group items that have several
tags? Put each tag as a header and list all items beneath it. That would be
good, but is it really needed?

I can probably come up with a use case for that. An item belongs both to `work'
and `ruby'. But how do I really group them? Is it really needed, several
classifications? `home' and `health' for a walk - totally unecessary.

Ok, let the users request it, if they need it, and opt for a simple solution in
the beginning.


## Add a new item to TODO or activity inventory

1. Input item name.
2. Input how many pomodoro it'll take (optional)
3. Choose category (optional).
4. Press button to put it into the list.
5. When it gets to the list, focus on it and flash yellow.


## Start using the app. Nothing at all is there.

1. Put everything into activity inventory.
2. Make a list for today by copying some items from activity inventory to TODO.
3. Start doing an item, a windows showing time left appears.
4. Play alarm when time elapses.
5. A way to stop playing alarm? A button "Start resting"?
6. Play alarm when rest ends.
7. A way to end alarm? Button "Back to TODO".


## A way to enter item when you remember it during pomodoro.

Have input fields just for that in the same window as timer. There should be
enough time. Having to switch between windows, I don't like that.


## Working on an item.

1. Scroll and select an item to work on. Press "start" button.
2. Timer window appears, and you're supposed to start working.
3. If you want to stop it, provide "stop" button. Hint or text below should
   explain that this pomodoro won't be counted as complete.
4. When alarm sounds, for how long it should be played? 1-2 minutes, I think.
5. Then, press "start resting" button, and a new timer appears.
6. Alarm after rest too. Stop it by pressing "back to TODO" button.


## Copying items from AI to TODO.

1. Given that AI is on the left, have a "copy ->" button. Category will be copied
   too.
2. Where will it appear? On the top? Better to put it on top, or put it sorted
   and scroll. Hell, what if our position in the list is somewhere in the
   middle? Well, it makes more sense to put it wherever it should be. Keeping it
   sorted is a simple way to do it. Although, people may want it to be
   different, but really?


## Calling it a day.

1. Have a button "call it a day".
2. When it's pressed, all items from TODO will be removed.


## Statistics.

1. Expected to take N pomodoro, but took Y pomodoro.
2. Completed pomodoro per day. Which timezone?


## Removing an item.

1. Press "delete" button.
2. Item disappears (if it's the only for the category, category disappears too).


## Move unsolved tasks back into AI.

I guess it should be a part of "calling it a day".


## Add a way to make a task "solved".

When you've finished working on a task, you want to mark it so you don't spend
time reading it (and can just scan the list to find tasks that aren't solved
yet). A checkbox or a button "mark solved" and "mark unsolved". Too long though,
so a checkbox might be good.


## Adding a task to AI.

When there's no category, add to the top. When there is category, add as the
first item inside category. This way for user it will be easy to find new tasks.


## Sorting of tasks.

AI and TODO sorting - allow for drag and drop.
