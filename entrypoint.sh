#!/bin/bash -e

RAILS_CMD="./bin/rails"

setup_db() {
  $(psql -l -h db -U postgres | grep -q "pomodoro_${RAILS_ENV}") \
    || $RAILS_CMD db:setup
  $RAILS_CMD db:migrate
}

setup_to_serve_static_files_in_production() {
  if [ "$RAILS_ENV" = "production" ]; then
    export RAILS_SERVE_STATIC_FILES=1
  fi
}

setup_db
setup_to_serve_static_files_in_production

$RAILS_CMD server
